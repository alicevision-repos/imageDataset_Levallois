# Levallois City Hall image dataset

![](http://www.popartproject.eu/images/mairie_levallois_9.png)

## The dataset
This dataset collects 599 synthetic images rendered from a 3D model of the [Levallois City Hall](https://goo.gl/maps/knYZXExjFEN2) (Paris, FRA) and 3 camera movements rendered by moving the Maya camera around the building.

## Dataset Structure
The dataset is composed of the following directories:

 - `scene` contains the 559 synthetic images generated with Maya;
 - `gt_scene` contains the ground truth data for the 559 images in `scene` folder;
 
 
## Comparing with the ground truth

In order to assess the quality of your reconstruction or your camera tracking 
result w.r.t. the provided ground truth you can use the script `openMVG_main_evalQuality` 
of the POPART fork of OpenMVG.
It takes as input a `.json` (or `.bin`) containing a reconstruction or, if supported,
an Alembic file (`.abc`).

As output it produces an html report `ExternalCalib_Report.html` containing the 
statistics for the baseline and angular error, and some `.ply` files showing the 
two camera sets aligned in the same reference frame. 

Here are the arguments of the script:

```
Usage: openMVG_main_evalQuality
[-i|--gt] path where ground truth camera trajectory are saved
[-c|--computed] path to a .json/.bin or an alembic file .abc containing the reconstruction or the localization result
[-o|--output] path (where statistics will be saved)
[-t|--camtype] Type of the camera:
 -1: autoguess (try 1,2,3),
  1: openMVG (bin),
  2: Strechas 'png.camera' 
  3: Strechas 'jpg.camera' ]
```

An example for comparing with the reconstruction of the folder `scene`:
```
openMVG_main_evalQuality --gt scene -computed sfm_data.json --output statistics
```


## License

This dataset is released under the Creative Commons Attribution-ShareAlike 3.0 Unported license (see [LICENSE.md](LICENSE.md))

## References

 - The POPART project: http://www.popartproject.eu
 - OpenMVG library (POPART fork): https://github.com/poparteu/openMVG 
 - Alembic open computer graphics interchange framework: http://www.alembic.io/
 - Maya® 3D animation, modeling, simulation, and rendering software: http://www.autodesk.com/products/maya/overview
 
## Citation

If you are referring to or using this dataset please cite it as 

```
POPART. (2015). POPART Virtual Dataset - Levallois Town Hall. Zenodo. DOI: 10.5281/zenodo.19164
```

BibTex
```
@misc{popart_2015_19164,
  author       = {POPART},
  title        = {POPART Virtual Dataset - Levallois Town Hall},
  month        = jun,
  year         = 2015,
  doi          = {10.5281/zenodo.19164},
  url          = {http://dx.doi.org/10.5281/zenodo.19164}
}
```

## Acknowledgements
This dataset has been developed during the [POPART](http://www.popartproject.eu/) project funded by the European Union’s Horizon 2020 research and innovation programme under grant agreement No 644874.