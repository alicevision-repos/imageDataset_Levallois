import maya.cmds as mc
import maya.OpenMaya as om

def mat2list(m):
    return [m(0,0),m(0,1),m(0,2),m(0,3),m(1,0),m(1,1),m(1,2),m(1,3),m(2,0),m(2,1),m(2,2),m(2,3),m(3,0),m(3,1),m(3,2),m(3,3)]

def copy_object_transformation(object_source, object_target, frame_start, frame_end):
    # Transformation matrix
    mat_trans = om.MFloatMatrix()
    om.MScriptUtil.createFloatMatrixFromList([1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], mat_trans)

    mc.select(object_target)

    for iframe in range(frame_start, frame_end + 1):
        mc.currentTime(iframe)
        mat_source = om.MFloatMatrix()
        mat_target = om.MFloatMatrix()
        om.MScriptUtil.createFloatMatrixFromList(mc.xform(object_source, worldSpace=True, query=True, matrix=True), mat_source)
        mat_target = mat_source * mat_trans
        mc.xform(object_target, worldSpace=True, matrix=mat2list(mat_target))
        mc.setKeyframe()




# Choose the first and last frame
iframe_start = 1
iframe_end = 374

# Choose target + source objects (use long names only)
# You can obtain the long name of an object by selecting it and running
# the MEL command : ls -l -sl

camera_source = '|group1|camera1_group|trackRender_MotionBlur'
camera_target = '|main|mvgRoot|mvgCameras|animxform_camera'
copy_object_transformation(camera_source, camera_target, iframe_start, iframe_end)

camera_source = '|group1|camera1_group|trackRender_MotionBlur|witnessCamera1'
camera_target = '|wc1|mvgRoot|mvgCameras|animxform_camera'
copy_object_transformation(camera_source, camera_target, iframe_start, iframe_end)

camera_source = '|group1|camera1_group|trackRender_MotionBlur|witnessCamera2'
camera_target = '|wc2|mvgRoot|mvgCameras|animxform_camera'
copy_object_transformation(camera_source, camera_target, iframe_start, iframe_end)